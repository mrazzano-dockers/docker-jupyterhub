How to use gitlab runners

# Stop all containers
docker container stop $(docker container ls -aq)
# remove all containers
docker container rm $(docker container ls -aq)

#start the runner
docker run -d --name gitlab-runner --restart always      -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

#then update configuration
docker restart gitlab-runner

#register
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register

Then check that
- the image is docker:stable
- that priviledge is true
 that the runner is not paused