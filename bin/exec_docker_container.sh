#!/bin/bash

## Check arguments
if [ $# -eq 0 ]
  then
    echo "No arguments supplied!"
    echo "run exec_docker_container_mmlab.sh BRANCH_NAME"
    echo "or:"
    echo "run exec_docker_container_mmlab.sh BRANCH_NAME test"
    exit 1
fi

#Define docker user name
DOCKER_USER_NAME=jovyan

#Read branch name
BRANCH_NAME=$1

#define name of the container
DOCKER_CONTAINER_NAME=docker-mmlab-${BRANCH_NAME}-${USER}

#Run tests
echo "Running test on Container ${DOCKER_CONTAINER_NAME}"
#docker exec -it --user studente ${DOCKER_CONTAINER_NAME} /bin/bash /opt/docker/test_shell_bash.sh
#docker exec -it --user studente ${DOCKER_CONTAINER_NAME} /bin/bash /opt/docker/test_shell_bash.sh

#docker exec -it --user ${DOCKER_USER_NAME} ${DOCKER_CONTAINER_NAME} /bin/bash /opt/docker/test_shell_bash.sh
#docker exec -it --user ${DOCKER_USER_NAME} ${DOCKER_CONTAINER_NAME} bash /opt/docker/test_python_env.sh

#Exec container
#echo "Exectuing Container ${DOCKER_CONTAINER_NAME}"
docker exec -it -w /home/${DOCKER_USER_NAME} --user ${DOCKER_USER_NAME} ${DOCKER_CONTAINER_NAME} /bin/bash
