#!/bin/bash

SCRIPT_NAME=pull_docker_image.sh
DOCKER_IMAGE_BASE_NAME=registry.gitlab.com/mrazzano-dockers/docker-jupyterhub
# Check arguments
if [ $# -eq 0 ]
  then
    echo "Error! No arguments supplied!"
    echo "run ${SCRIPT_NAME} BRANCH_NAME"
    exit 1
fi

#Read branch name as argument
BRANCH_NAME=$1

echo "Login to Docker Registry..."
echo "Enter your Gitlab.com username and password"
docker login registry.gitlab.com

echo "Now pulling the image from the Registry..."
docker pull ${DOCKER_IMAGE_BASE_NAME}:${BRANCH_NAME}
echo "Docker pull succeeded!"