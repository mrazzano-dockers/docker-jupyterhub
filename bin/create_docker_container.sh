#!/bin/bash

# Check arguments
if [ $# -eq 0 ]
  then
    echo "No arguments supplied!"
    echo "run create_docker_container_mmlab.sh BRANCH_NAME"
    exit 1
fi

#Read branch name as argument
BRANCH_NAME=$1
DOCKER_USER_NAME=jovyan
DOCKER_IMAGE_NAME=registry.gitlab.com/mrazzano-dockers/docker-mmlab:${BRANCH_NAME}
    
#Create a Docker container
echo "Create new Docker container for mmlab using branch ${BRANCH_NAME}"
echo "HOME Directory is ${HOME}"

#Create the work directory
echo "Creating work-mmlab directory..."
host_workdir=${HOME}/work-${USER}

if [[ -d ${host_workdir} ]]
then
    echo "${host_workdir} already exists. skip creation!"
else
    mkdir -p ${host_workdir}
fi

#Stop previous containers and delete them
DOCKER_CONTAINER_NAME=docker-mmlab-${BRANCH_NAME}-${USER}
xhost +local:

echo "Removing previous containers named ${DOCKER_CONTAINER_NAME}..."
docker container stop ${DOCKER_CONTAINER_NAME}
docker container rm ${DOCKER_CONTAINER_NAME}


echo "Creating Docker container named ${DOCKER_CONTAINER_NAME}..."
#check the user id
HOST_USER_ID=`id -u $USER`
if [ $USER == "root" ]; then
    HOST_USER_ID=1040
    echo "Warning! For user root HOST_USER_ID set to ${HOST_USER_ID}"
fi
   
docker create -it --init \
-e HOST_USER_ID=${HOST_USER_ID} \
-e DOCKER_USER_NAME=${DOCKER_USER_NAME} \
-e DISPLAY=${DISPLAY} \
-v ${host_workdir}:/home/${DOCKER_USER_NAME}/work \
-v /home/${USER}/.ssh:/home/${DOCKER_USER_NAME}/.ssh \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-p 8888:8888 \
--name ${DOCKER_CONTAINER_NAME} \
${DOCKER_IMAGE_NAME}

echo "Starting Docker container named ${DOCKER_CONTAINER_NAME}..."
docker start ${DOCKER_CONTAINER_NAME}
echo "Showing logs for ${DOCKER_CONTAINER_NAME}..."
docker logs --details --tail -99 ${DOCKER_CONTAINER_NAME}
echo "Creation done!"
