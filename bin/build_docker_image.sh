#!/bin/bash

# Check arguments
if [ $# -eq 0 ]
  then
    echo "No arguments supplied!"
    echo "run build_docker_image-mmlab.sh BRANCH_NAME"
    exit 1
fi

#docker container prune --force
#docker image prune --force

#Read branch name as argument
BRANCH_NAME=$1

#Build Docker image
echo "Read branch $BRANCH_NAME"
DOCKER_IMAGE_NAME=registry.gitlab.com/mrazzano-dockers/docker-mmlab:${BRANCH_NAME}

echo "Building Docker image ${DOCKER_IMAGE_NAME}"
docker build --rm --no-cache --build-arg BRANCH_NAME=${BRANCH_NAME} -f ${BRANCH_NAME}/Dockerfile -t ${DOCKER_IMAGE_NAME} .
echo "Docker Image Building done!"
