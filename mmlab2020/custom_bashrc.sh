#ENV variables
# Environment variabless" >> $DEST_BASHRC
# export ASTROSW_DIR=${ASTROSW_DIR}" >> ${DEST_BASHRC}
# echo "export HEADAS=${HEADAS}" >> ${DEST_BASHRC}
#echo "export PATH=${PATH}:\$PATH" >> ${DEST_BASHRC}

export MINICONDA_DIR=/opt/miniconda3
#Aliases
#echo "alias notebook='jupyter notebook --ip 00.00.00.00 --no-browser'" >> ${DEST_BASHRC}
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias ls='ls --color=auto'
alias photon-env-py3='conda activate photon-env-py3'
#alias gw-env-py3='conda activate gw-env-py3'
alias deactivate='conda deactivate'

# Set terminal colors" >> ${DEST_BASHRC}
#dark
# PS1='\[\033[0;32m\](docker)\[\e[1;33m\]\u@\H $ \[\033[1;37m\]'
#light
#PS1='\[\033[0;32m\](docker)\[\e[1;31m\]\u@\H $ \[\033[1;34m\]'
PS1='\[\033[0;32m\]\[\e[1;31m\]\u@\H $ \[\033[1;34m\]'
COLOR_TERM_YELLOW='\033[1;33m'
COLOR_TERM_NC='\033[0m'
COLOR_TERM_GREEN='\033[1;36m'

#Welcome message
echo "*********************************************************************"
echo -e "**   ${COLOR_TERM_GREEN}Welcome to the Multimessenger Physics Lab 2020 Online Container${COLOR_TERM_NC}    **"
echo "*********************************************************************"
#echo -e "* ASTROSW_DIR is ${COLOR_TERM_YELLOW}\${ASTROSW_DIR}\${COLOR_TERM_NC}"
echo
echo -e "* To setup Python Photon Environment type: ${COLOR_TERM_GREEN}'photon-env-py3'${COLOR_TERM_NC}. To exit the Environment, type 'conda deactivate'"
#echo -e "* To setup Python Gravitational Wave Environment type: ${COLOR_TERM_GREEN}'gw-env-py3'${COLOR_TERM_NC}. To exit the Environment, type 'conda deactiate'"
echo -e "* To deactivate Conda environment type ${COLOR_TERM_GREEN}'deactivate'${COLOR_TERM_NC}."
#echo -e "* To run Jupyter Notebook, first activate the photon-env-py27 or gw-env-py27 Python environment and then type ${COLOR_TERM_YELLOW}'notebook'\${COLOR_TERM_NC} and copy the suggested URL in your host browser"
source ${MINICONDA_DIR}/etc/profile.d/conda.sh
echo
cd
