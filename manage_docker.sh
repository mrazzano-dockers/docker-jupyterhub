#!/bin/bash

SCRIPT_NAME=manage_docker.sh
DOCKER_IMAGE_BASE_NAME=registry.gitlab.com/mrazzano-dockers/docker-jupyterhub
MANAGER_VERSION="0.1"
DOCKER_USER_NAME=gwmluser

echo
echo "***********************************************"
echo "**    Docker Nvidia Manager (Version ${MANAGER_VERSION})    **"
echo "***********************************************"
echo

# Check arguments
if [ $# -eq 0 ]
  then
    echo "Error! No arguments supplied!"
    echo
    echo "Usage:"
    echo "To pull image: ${SCRIPT_NAME} pull BRANCH_NAME"
    echo "To create container: ${SCRIPT_NAME} create BRANCH_NAME"
    echo "To execute container: ${SCRIPT_NAME} exec BRANCH_NAME"
    echo
    exit 1
fi

#Read branch name as argument
RUN_MODE=$1
BRANCH_NAME=$2


HOST_USER_ID=`id -u $USER`
if [ $USER == "root" ]; then
    HOST_USER_ID=1040
    echo "Warning! For user root HOST_USER_ID set to ${HOST_USER_ID}"
fi

#Build the name of the container
DOCKER_CONTAINER_NAME=docker-nvidia-ml-${BRANCH_NAME}-${USER}

if [ "${RUN_MODE}" = "pull" ]; then
    echo "Pulling branch ${BRANCH_NAME}..."
    echo "Login to Docker Registry..."
    echo "(You might be asked for your Gitlab user and pasword)"
    docker login registry.gitlab.com
    docker pull ${DOCKER_IMAGE_BASE_NAME}:${BRANCH_NAME}
elif [ "${RUN_MODE}" = "create" ]; then
    xhost +local:
    echo "Removing previous containers named ${DOCKER_CONTAINER_NAME}..."
    docker container stop ${DOCKER_CONTAINER_NAME}
    docker container rm ${DOCKER_CONTAINER_NAME}
    host_workdir=${HOME}/work-docker-nvidia-${USER}
    echo "Creating Shared work directory ${host_workdir}..."
    # Creating Docker container
    mkdir -p ${host_workdir}
    echo "Creating Docker container named ${DOCKER_CONTAINER_NAME}..."
    # Creating Docker container
    docker create -it --init  --gpus all -e HOST_USER_ID=${HOST_USER_ID} \
    -e DOCKER_USER_NAME=${DOCKER_USER_NAME} \
    -e DISPLAY=${DISPLAY} \
    -v ${host_workdir}:/home/${DOCKER_USER_NAME}/work \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -p 8888:8888 \
    --name ${DOCKER_CONTAINER_NAME} \
    ${DOCKER_IMAGE_BASE_NAME}:${BRANCH_NAME}
    #    -v /home/${USER}/.ssh:/home/${DOCKER_USER_NAME}/.ssh \
    #Start!
    echo "Starting Docker container named ${DOCKER_CONTAINER_NAME}..."
    docker start ${DOCKER_CONTAINER_NAME}
    echo "Showing logs for ${DOCKER_CONTAINER_NAME}..."
    docker logs --details --tail -99 ${DOCKER_CONTAINER_NAME}

elif [ "${RUN_MODE}" = "exec" ]; then
    echo "Executing container ${DOCKER_CONTAINER_NAME}..."
    docker exec -it -w /home/${DOCKER_USER_NAME} --user ${DOCKER_USER_NAME} ${DOCKER_CONTAINER_NAME} /bin/bash
else
    echo "Command ${RUN_MODE} not recognized!"
    exit 1
fi








#echo "Now pulling the image from the Registry..."
#echo "Docker pull succeeded!"
